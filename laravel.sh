LARAVEL COMMANDS
-create laravel project: composer create-project laravel/laravel=version folder-name
-list of commands: php artisan
-start development server: php artisan serve
-start development server (using local ip address and custom port): php artisan serve --host 192.168.5.67 --port 9090
-start development server (without artisan):
cd public
php -S localhost:8000
-see detail of commands: add --help e.g. php artisan serve --help
-check laravel version: php artisan serve --version

LARAVEL PROJECT
-public/index.php: entry point, all requests will through this file, only public folder is exposed out
-from index.php will be continued to class kernel
-there are 2 kind of kernel: http kernel and console kernel
-http kernel is used to crate web based app
-console kernel is used for create terminal based app
-kernel is core logic of app, bootstraping which run service provider
-service provider bootstraping all components like database  queue, validation, routing etc.
-location of service provider: app/Providers
-for now we will learn app/Providers/RouteServiceProvider.php
-there are some routes like web, api
-create route:
Route::get('/', function(){
 return view('welcome');
});
-testing: unit test and feature test/integration test-unit test: use derived class PHPUnit\Framework\TestCase
-integration test: use derived class Illuminate\Foundation\Testing\TestCase
-create feature test: php artisan make:test ContohTest
-create unit test: php artisan make:test ContohTest --unit
-run unit test: php artisan test or vendor/bin/phpunit tests/Feature/EnvTest.php
-environment: use function env(key) or Env::get(key)to get value of the environment
-set environment variable on system: export ENV_NAME="VALUE"
-get value of environment variable on system: echo $ENV_NAME
-remove environment variable on system: unset ENV_NAME












